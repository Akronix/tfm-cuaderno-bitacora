**Tareas para coger**
* [ ] Escribir en blog progreso hasta ahora

_Baja prioridad_
* [ ] Mejora en código: Coger trozos validación y test al azar (inspirarse en código k-fold validation o usar generador). Dará un RMSE más fiable.
* [ ] Verificar correlación entre tráfico y contaminación (en particular, de NOx) para justificar usarlo como variable de entrada al modelo (notebook separado en R) -> Cambiar por referencias a biblio
* [ ] Hacer k-fold validation para experimento Talos y así evaluar mejor el modelo
* [ ] Usar datos normalizados y evaluar resultados así.


# Cuaderno de Bitácora

**9 de Junio**

_Check-in_

- [X] Terminar memoria. Revisar todo y entregar.

**8 de Junio**

_Check-out_

Hasta aquí va a ser la entrega de la PEC3 en cuanto a desarrollo. Mañana termino de retocar la memoria y lo entrego 🎉.
Lo del mapa en tiempo real lo iré haciendo a poquitos y entregaré lo que dé tiempo para la PEC4 del TFM.

_Check-in_
- [X] Escribir en memoria todo lo hecho hasta ahora
- [X] Preguntar a profesor GIS cómo mejor mostrar mapa con interpolación: Shiny? carto?
- [ ] Mapa carto con resultados práctica GIS

**7 de Junio**

_Check-out_
- Terminados los modelos All-in-one. Probadas diversas configuraciones de parámetros.
- Empezado a mirar / pensar como mostrar mapa en tiempo real

_Check-in_

Hoy termino la semana y también es el último día que me propuse para trabajar en el proyecto. Ademá, es domingo y llevo mucho tiempo sin ver a mis amigos, así que estoy pensando en descansar
un porquito.

- Meter más datos estaciones meteo en All-in-one 2.x.
- Intentar conseguir buen rendimiento con aio 1.x y 2.x -> quizás tenga que usar el pylab del servidor
- Mirar cómo hacer para hacer entrenamiento y predicciones en tiempo real (bucle infinito y recargar cada 15 min?)

**5 de Junio**

_Check-out_
- Terminado (por fin!) sección predicciones 8, 16 y 24 y terminada todo de la memoria al respecto.
- Decidido priorizar modelo all-in-one por encima de prototipo 3 con datos de tráfico por utilidad futura con mapa en web.

**3 de Junio**

_Check-in_
- Acabar predicciones 8, 12 y 24h y escribir memoria.

**2 de Junio**

_Check-in_
- Terminar refactor
- Mejorar predicciones 24 horas 
- Escribir memoria
- Hacer predicción 8 y 12 horas -> si da tiempo

_Check-out_
- Terminado refactor
- Añadidos datos 1 año a predicciones 24h -> todavía son malos resultados...No sé porqué y ahora dudo si fue buena idea meterlos...

**2 de Junio**

_Check-in_
- Hacer predicciones 8, 12 y 24 horas
- Escribirlo en memoria
- Refactorizar código partir conjuntos auxiliares

_Check-out_
- Hecho predicciones 24 horas con modelo SVR y 3 prototipos. ligera optimización de hiperparámetros
- refactorizado código en utils de partir todo el df. Todavía falta por adaptar código a esta nueva función y testear.

**1 de Junio**

Retomo el TFM en su última semana. Objetivo: Conseguir algo úti, difundible y presentable al CUSL.

_Check-in_
- Contestar a tutor
- Pequeños arreglos en la memora
- Planificar última semana:
    1. Continuar Trabajo de Desarrollo hasta día 7.
    2. Día 8 y 9 retoques memoria
    3. A partir de día 10 centrarme en fundamentos
- Comenzar predicción 24 horas

**25 de Mayo**

_Check-out_
- Hecha tabla resultados final prototipo 2.

**22 de Mayo**

_Check-out_
- Terminada la escritura de todo menos la tabla del final del prototipo 2! Mañana ya estará todo listo para una primera revisión :D

_Check-in_
- No he dormido bien y me despierto con el cerebro sin funcionar. Tengo que remontar el día como sea y volver al ritmo de antes.
- Intentar terminar memoria de una vez y enviársela a Sergi.

**21 de Mayo**

_Check-out_
- Aparte de ver la charla de EeA sobre calidad del aire y la planificación para las próximas semanas y asignaturas, no hice nada de nada :(
- La movida personal va a peor y me ha frenado en seco.

_Check-in_
- Estoy bloqueado mentalmente entre una movida personal ocurrida recientemente y desde que han aplazado la entrega de la PEC.
- Tengo que revisar la planificación y ver cómo me voy a organizar ahora. Hay que tener en cuenta la desescalada y las otras asignaturas.
- Con todo, tengo que mantener mi progreso constante de cada día sobre la memoria. Hoy debería de terminar de una vez el capítulo para el modelo.
Aunque el día no augura ser productivo porque me voy a poner tarde y además hay una charla de EeA sobre la calidad del aire hoy a las 18:00h.

**20 de Mayo**

_Check-out_

Avances no demasiados pero sí he hecho bastante. Conseguido objetivo de 5 páginas escritas al día.
Me ha descolocado y desmotivado saber lo de la extensión de la fecha de entrega de la PEC.

- Memoria: Terminada sección modelos base.
- Memoria: Hecha primera parte prototipo 0.
- Probado experimento de poner 1024 units a cada capa de la red LSTM. Los resultados son desastrosos.
- 20 páginas al finalizar de modelo.tex!

_Punto técnico_

16:30 -> Acabo de recibir la noticia de que se extienden los plazos del TFM...En realidad no es una buena noticia para mí, no quiero replantear mi planificación y me gustaría haber acabado
finalmente para medaidos de Junio, tal y como estaba planeado. Pensaré próximamente si pedirle al tutor que mantenga mis fechas.

_Check-in_

Hoy empiezo tarde y con pocas fuerzas. Tengo que remontar el día.
¡A escribir!

Empiezo con 15pags de modelo.tex escritas

**19 de Mayo**

_Check-out_
- Memoria: Terminado capítulo datos entero.
- Memoria: Añadido anexo con diagramas de diseño.
- Memoria: Avamzada bastante la parte del prototipo 2.
- Hecho también test de Dickey-Fuller aumentado y sacado análisis de series temporales a otro notebook.

Buen ritmo! Sigue así!

_Check-in_

Seguir escritura memoria lo máximo posible.


**18 de Mayo**

_Check-out_
- Arreglado que los datos aire no correspondían con la misma estación que los datos meteo
- Escrito análisis estadístico meteo
- Hechos experimentos con hiperparámetros para prototipo 2


_Check-in_

Última semana antes de entrega de implementación. Prioridades:
1. Escribir todo lo hecho hasta ahora en la memoria lo mejor posible.
2. Añadir lo más interesante de lo que falte e incluirlo.

**17 de Mayo**

_Check-out_

Implementado prototipo 2, aunque no con mejores resultados por el momento.


_Check-in_

Hoy no hay tiempo que perder, tengo que tener hecho el Prototipo 2.

**16 de Mayo**

_Check-out_

Apenas empecé a modificar el script de carga de datos, pero me quedé bloquedado. Día **muy** poco productivo

_Check-in_

Hoy comienzo tarde a trabajar, así que no me voy a poner demasiadas tareas.

- [X] Seleccionar estacion meteo y aire cercanas
- [ ] Hacer script que selecciona datos.
- [ ] Documentar todo el proceso anterior en la memoria.
- [ ] Si diera tiempo: hacer diseño prototipo 2


**15 de Mayo**
_Check-out_
- Terminado a las 20:20. Bien por ceñirte al horario.
- Buen progreso y buenos resultados semanales.
- Una semana para acabar, yay!

_Check-in_
* [X] Hacer que funcione prototipo 1 con entradas auxiliares
* [X] Documentar en la memoria
* [X] Enviar lo que tengo hasta ahora al tutor y mis ideas última semana
* [ ] Prototipo 2: Ir mirando ya como meter meteo y empezar con ello -> Esperar a respuesta tutor
* [X] Importante seguir horario y acabar a la hora


**14 de Mayo**

_Check-out_
- Todo el día intentando hacer que funcione el modelo con varios inputs...sin éxito aunque me he quedado cerca. Tendré que preguntar en SO o algo así.
- No he hecho nada de la memoria y he acabado demasiado tarde.

_Check-in_
* [X] Añadir variables auxiliares: Día de la semana, hora del día.
    * [X] Cambiar script selección de datos
    * [X] Cambiar diseño modelo
* [ ] Continuar escritura de la memoria.
* Si sobra tiempo ir mirando como meter meteo o estudio estacionariedad series temporales mediante test Dickey-Fuller


**13 de Mayo**

_Check-out_
- Productivo por la mañana con la refactor de SVR y sacando interesantes resultados
- Pensado y decidido diseño para meter meteo.
- No ha sido posible meter meteo porque la estación de meteo del ayto que iba a coger tiene pocas magnitudes disponibles y los de la AEMET no están disponibles por horas (solo por días).
- Leído libro de keras cuando estaba ya cansado. Poco productivo.
- He pensado que como paso intermedio antes de meter meteo quizás sea más interesante, tanto para evaluar la mejoras como para mi aprendizaje, que el prototipo 1 consista en añadir variables auxiliares
y ya dejar para el prototipo 2 meter las variables meteo. -> Decidir mañana antes de ponerse con uno o con otro.

_Check-in_
* [X] Refactorizar SVR
* [X] Leer sobre añadir más inputs (libro keras)
* [ ] Añadir variable meteo



**12 de Mayo**

_Check-out_

_(Escribí un checkout más largo ayer pero se ha perdido :( )_

- Cambio de rumbo: haré solamente previsión a 1h en lugar de las 24 horas posteriores. De este modo, me doy más tiempo en hacer un modelo más interesante, con más entradas y, también para poderlo comparar con otros artículos, que veo que muchos son a 1h, y el modelo SVR que ya tengo hecho. 
- Es mucho mejor ir escribiendo memoria al mismo tiempo que haciendo experimentos. También ir generando gráficas y guardando resultados. -> A partir de ahora escribiré la memoria a diario

_Check-in_
* [ ] Leer sobre añadir más inputs (libro keras)
* [ ] Añadir variable meteo
* [X] Leer sobre predicción de secuencias en el futuro.
* [?] Continuar escritura de la memoria (un poco)

**11 de Mayo**

_Check-out_
- Día poco productivo. El cambio de tiempo me afecta...
- Comencé escritura de la memoria y, aunque aún queda mucho por hacer, estuvo bien sentir que voy avanzando en ese frente.
- Por cuestiones de todo lo que he tenido que aprender hasta el momento, el tiempo que tenía, y, en especial, el orden en el que he podido hacer las cosas (sin tener guía de cómo hacerlo);
no me va a dar tiempo a hacer todo lo que quiero hacer a tiempo. Así que es mejor asumirlo y ponserse unos objetivos más humildes, pero tener una buena memoria donde se explique con detalle
todo lo que he hecho.

_Check-in_
* [X] Revisar comentarios tutor sobre diseño.
* [X] Pequeños cambios en el código: cambiar nombre de `sequence_length` a `window_width`,
cambiar monitorización de métrica mae por mape y mostrarla también en las gráficas de resultados.
* [?] Documentar todo lo que he hecho y conseguido hasta ahora. Cierre iteración 0.

**9 de Mayo**

_Check-out_
- De nuevo, me he entretenido mucho con una cosa que no sé hasta que punto es necesaria / útil: El hallar el patrón de estacionalidad de los datos.
- Útil haber vuelto a apuntes sobre series temporales de la asignatura de minería avanzada
- Para los datos no verificados (_missing values_), de momento replico el valor anterior. No es lo mejor claramente, pero no es demasiado malo dada la pequeña cantidad de datos que son.
Es mucho mejor que borrar esos datos porque, si los borro, las secuencias pierden su periodicidad y empiezan de nuevo.
    - Esto se corregiría cuando incluya la hora del día en el que estamos. Una vez haya incluido dicho input auxiliar, ya podría borrar los datos antes de metérselo de entrada al modelo.
- Análisis básico en cuanto a estadísticos, pero extenso en cuanto a análisis de series temporales. Falta un poco más de interpretación de los resultados en forma de markdwon dentro del notebook.
- A ver como resumo / escribo tantas cosas en la memoria...

_Check-in_
* [X] Separar código modelo NN de lectura y selección de datos. Preparar más datos de entrada al modelo
* [X] Realización de análisis estadísticos en los datos
* [X] Tratar los missing values
* [ ] Pequeños cambios en el código: Coger trozos validación y test al azar (inspirarse en código k-fold validation), cambiar nombre de `sequence_length` a `window_width`

**8 de Mayo**

_Check-out_

- Me he distraído mucho preprando la normalización de los datos de entrada, y luego he pensado que realmente no es tan interesante dado que todos los datos están en la misma escala,
y complica las cosas a la ahora de evaluar el modelo contra el test set. De hecho, justo para el mes que he escogido, los datos tienen picos muy espúreos, de modo que la normalización
ahonda todavía más esa diferencia
- Muy útil e interesante la lecturas del libro. Uso inmediato en mi código.
- No he hecho todavía lo de soportar meter más datos y es algo ya imprescindible para empezar a poder usar en serio el modelo. URGENTE!!!
- He pensado un poco acerca el tema de los missing values (he pensado que lo óptimo sería hacer interpolación siguiendo la "forma" que tuvieran los datos 24 horas antes
Con esto, estamos suponiendo peiodicididad de 24h, que es algo que habría que comprobar también. Otras opciones más sencillas es: ignorar esos datos (lo que estoy haciendo a día de hoy),
usar la media, interpolar su valor entre los valores anteriores (usando una recta o una distribución normal de los valores usando la mean y std de los valores de los datos).
En fin, quizás demasiada complicación para unos casos muy puntuales.
* [X] Invertir orden secuencias (más reciente primero). Probar también LSTM bidireccional (como Xiang Li, 2017). 
-> Al último momento también he probado a invertir las secuencias como se sugiere en 6.3.8. de la 1ª edición del libro, para ver si vale la pena probar LSTM direccional.

_Check-in_
* [X] Leer capítulo 5 libro: "Fundamentals of machine learning". Leído también casi todo del capítulo 6 de la primera edición: "Chapter 6. Deep learning for text and sequences"
* [ ] Separar código modelo NN de procesamiento de datos. Preparar más datos de entrada al modelo
* [X] Refactor code of notebooks. Aprovechar para Repasar código stateful (bucle), para ver si es correcto. Agrupar posibles parámetros en diccionario "params".
Normalizar datos?
* [?] Pensar cómo tratar los missing values

**7 de Mayo**

_Check-out_
- Pensado bastante acerca del diseño del modelo. Planteadas preguntas fundamentales y reflexionado sobre sus respuestas.
- He leído el capítulo 3 del libro y (mayormente re-)leído el capítulo 4. Muy muy, relevantes para lo que estoy haciendo. ¿Quizás debería haberlos leído antes y así 
habría entendido mejor lo que estoy haciendo, de cara a hacer experimentos y pruebas con sentido? -> Para mañana quiero leer el capítulo 5 antes de meterme en ninguna modificación más del modelo.
- No ha sido un día demasiado productivo en cuanto a cumplir tareas. Tampoco he dedicado las 8 horas que me dije que dedicaría. Mañana esto tendrá que cambiar.
- Creo que necesito más datos antes de hacer pruebas con n_sequence del modelo, puesto que con solo un mes los resultados pueden ser no muy representativos. ->
 La idea es buscar una manera de evaluar también cuál es el n_sequence (window length) más óptimo para mi caso, pero como esto puede cambiar cuando el pronóstico sea a 24 horas no interesa perder
mucho tiempo en eso ahora.
- Aunque he pensado en el diseño, todavía tengo que hacer la refactorización de código para permitir: cambios fáciles y rápidos en hiperparámetros, separar lectura y formateo de datos vs de 
construcción del modelo, tener en cuenta que sea fácil de modificar _el window length_ mencionado en el punto anterior, añadir paso de normalización sobre el train set.
- Ah! También he estado con el tema de la gestión de la petición del servidor y sus especificaciones técnicas

_Check-in_
* [X] Evaluar modelo top 1 Talos con n_sequence de 8
* [X] Plantear dudas diseño a tutor y a sabedor de Keras
* [X] Leer capítulo 3 y 4 libro: "Introduction to Keras and TensorFlow"


**6 de Mayo**

_Check-out_
- En la reunión con el tutor hemos hablado de pedir una MV para el TFM.
- Hecho diagrama prototipo 0
- Guardados resultados de experimento talos anterior y ejecutado con mejores parámetros prototipo 0.2 Guardados resultados.
- Probado stateful y stateless con num_seq = 8 en lugar de 24. Guardados resultados.


_Check-in_
* [X] Hacer diagramas diseño modelo.
* [X] Pensar cómo incluir en el diseño más variables. Pensar acerca de otras cuestiones de diseño (LSTM stateful, LSTME, etc.).
* [X] Evaluar modelo top 1 Talos
* [ ] Leer capítulo 3 libro: "Introduction to Keras and TensorFlow"
* [X] Reunión con tutor

**5 de Mayo**

_Check-out_
- Hecho prototipo 0.3 que usa LSTM stateful
- Leído artículo LSTM stateful: http://philipperemy.github.io/keras-stateful-lstm/
- Leído capítulo 2 libro: "The mathematical building blocks of neural networks". También bastante del 4: "Getting started with neural networks"
- Normalización de los datos antes de metérselos al modelo
- Ejecutado Talos con 108 combinaciones de parámetros. Guardados resultados para contraste posterior.
- Cambios menores en código


_Check-in_
* [ ] Hacer diagramas diseño modelo.
* [X] Pensar sobre diseño modelo - LSTM stateful. Mirar LSTME, entradas tráfico y meteo.
* [X] Leer docu y optimizar parámetros con Talos.
* [X] Seguir leyendo libro keras.


**4 de Mayo**

_Check-out_
- Todo el día gastado en intentar hacer funcionar CUDA 10 en mi GPU Nvidia 710M, sin éxito.
- Leído un poco libro keras.


**1 de Mayo**

_Check-out_
- Hecho modelo SVR con RMSE de 39.815.

_Check-in_
* [X]  Enviar una versión buena del Protopipo 0.2 al tutor
* [X]  Probar Hyperparameter Optimizators (probablemente talos)
* [X]  Modelo ARMA para comparar desempeño -> Al final conseguí SVR
* [ ]  LSTM stateful que use historial de 24 horas anteriores. -> pendiente de investigar en qué casos y cómo usar LSTM stateful.
* [ ]  Preparar más datos para meter de entrada al modelo.

**30 de Abril**

_Check-out_
- Mejorado Prototipo 0.2 con tuning de parámetros. Buena generación de conjuntos de testing y de training
- Bastante formación en keras.
- Los mejores resultados obtenidos con LSTM 1000 x 1000 y 100 epochs
- Me queda la sospecha de que debería ser LSTM stateful
- Todavía hay cosas que me falta entender *bien*: batch_size, LSTM stateful vs LSTM stateless, shuffle en model.fit, buenos valores para EarlyStopping

